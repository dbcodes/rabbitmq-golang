/*
This consumer creates a named queue with several bindings.
*/
package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/spf13/viper"
	"github.com/streadway/amqp"
)

type Consumer struct {
	conn    *amqp.Connection
	channel *amqp.Channel
	tag     string
	done    chan error
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func handle(deliveries <-chan amqp.Delivery, done chan error) {
	for d := range deliveries {
		var msg map[string]interface{}
		if err := json.Unmarshal(d.Body, &msg); err != nil {
			failOnError(err, "JSON error")
		}
		log.Printf(
			"got %d delivery {%v}: [%v] %s",
			len(d.Body),
			d.RoutingKey,
			d.DeliveryTag,
			msg,
		)
		d.Ack(false)
	}
	log.Printf("handle: deliveries channel closed")
	done <- nil
}

func main() {
	// configuration
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		failOnError(err, "Failed to found configuration")
	}
	amqpURL := viper.GetString("consumer.url")
	exchangeName := viper.GetString("consumer.exchange.name")
	exchangeType := viper.GetString("consumer.exchange.type")
	exchangeDurable := viper.GetBool("consumer.exchange.durable")
	queueName := viper.GetString("consumer.queue.name")
	queueDurable := viper.GetBool("consumer.queue.durable")
	queueAutodelete := viper.GetBool("consumer.queue.autodelete")
	queueExclusive := viper.GetBool("consumer.queue.exclusive")
	queueArguments := viper.GetStringMap("consumer.queue.arguments")
	bindings := viper.GetStringSlice("consumer.bindings")
	// consumer object
	c := &Consumer{
		conn:    nil,
		channel: nil,
		tag:     viper.GetString("consumer.consumerTag"),
		done:    make(chan error),
	}
	// rabbitmq connection
	c.conn, err = amqp.Dial(amqpURL)
	failOnError(err, "Failed to connect to RabbitMQ")
	go func() {
		log.Printf("closing: %s", <-c.conn.NotifyClose(make(chan *amqp.Error)))
	}()
	// create channel
	c.channel, err = c.conn.Channel()
	failOnError(err, "Failed to open a channel")
	// declare exchange
	err = c.channel.ExchangeDeclare(
		exchangeName,    // name
		exchangeType,    // type
		exchangeDurable, // durable
		false,           // auto-deleted
		false,           // internal
		false,           // no-wait
		nil,             // arguments
	)
	failOnError(err, "Failed to declare an exchange")
	// declare queue
	//type Table map[string]interface{}
	fmt.Println(queueArguments)
	q, err := c.channel.QueueDeclare(
		queueName,       // name
		queueDurable,    // durable
		queueAutodelete, // delete when usused
		queueExclusive,  // exclusive
		false,           // noWait
		queueArguments,  // arguments
	)
	failOnError(err, "Failed to declare a queue")
	// bind queue
	for _, s := range bindings {
		log.Printf("Binding queue %s to exchange %s with routing key %s", q.Name, exchangeName, s)
		err = c.channel.QueueBind(
			q.Name,       // queue name
			s,            // routing key
			exchangeName, // exchange
			false,        // noWait
			nil)          // arguments
		failOnError(err, "Failed to bind a queue")
	}
	// consume messages
	msgs, err := c.channel.Consume(
		q.Name, // name
		c.tag,  // consumerTag,
		false,  // noAck
		false,  // exclusive
		false,  // noLocal
		false,  // noWait
		nil,    // arguments
	)
	failOnError(err, "Failed to register a consumer")

	go handle(msgs, c.done)

	forever := make(chan bool)

	log.Printf(" [*] Waiting for logs. To exit press CTRL+C")
	<-forever
}
