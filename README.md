# rabbitmq-golang

Docker to run RabbitMQ.  

    $ docker-compose up

Install dependencies.  

    $ go get github.com/streadway/amqp
    $ go get github.com/spf13/viper

Consumer and Producer sharing the same configuration defined in `config.json`.  

Start the consumer.  

The `consumer.go` prints out the arriving messages.    

    $ go run consumer.go
    2018/11/28 21:51:52 Binding queue logs to exchange api.logs with routing key *.*.*
      [*] Waiting for logs. To exit press CTRL+C
    got 46 delivery {user.create.account}: [1] map[title:Golang messaging id:1234567890]
    got 46 delivery {user.create.account}: [2] map[title:Golang messaging id:1234567890]

Send messages with the producer.

    $ go run producer.go <routing-key>

    $ go run producer.go user.create.account
    $ go run producer.go user.update.account

    $ go run producer-loop.go user.create.account
