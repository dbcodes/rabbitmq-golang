package main

import (
	"encoding/json"
	"log"
	"os"
	"time"

	"github.com/spf13/viper"
	"github.com/streadway/amqp"
)

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func main() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		failOnError(err, "Failed to found configuration")
	}
	amqpURL := viper.GetString("producer.url")
	contentType := viper.GetString("contentType")
	expiration := viper.GetString("producer.expiration")
	exchangeName := viper.GetString("producer.exchange.name")
	exchangeType := viper.GetString("producer.exchange.type")
	exchangeDurable := viper.GetBool("producer.exchange.durable")
	// create connection
	conn, err := amqp.Dial(amqpURL)
	failOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()
	// create channel
	ch, err := conn.Channel()
	failOnError(err, "Failed to open a channel")
	defer ch.Close()
	// declare exchange
	err = ch.ExchangeDeclare(
		exchangeName,    // name
		exchangeType,    // type
		exchangeDurable, // durable
		false,           // auto-deleted
		false,           // internal
		false,           // no-wait
		nil,             // arguments
	)
	failOnError(err, "Failed to declare an exchange")
	// send message
	msgString := map[string]string{"id": "1234567890", "title": "Golang messaging"}
	msgJSON, _ := json.Marshal(msgString)
	routingKey := severityFrom(os.Args)
	for {
		err = ch.Publish(
			exchangeName,
			routingKey,
			false, // mandatory
			false, // immediate
			amqp.Publishing{
				Headers:         amqp.Table{},
				ContentType:     contentType,
				ContentEncoding: "utf8",
				DeliveryMode:    amqp.Transient, // 1=non-persistent, 2=persistent
				Priority:        0,              // 0-9
				Expiration:      expiration,
				Body:            msgJSON,
			})
		failOnError(err, "Failed to publish a message")
		log.Printf(" [x] Publishing %s %s %dB body (%s)", exchangeName, routingKey, len(msgJSON), msgJSON)
		time.Sleep(time.Second)
	}
}

func severityFrom(args []string) string {
	var s string
	if (len(args) < 2) || os.Args[1] == "" {
		s = "anonymous.info"
	} else {
		s = os.Args[1]
	}
	return s
}
